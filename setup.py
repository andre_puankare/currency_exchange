import os
from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()


requires = [
    'pyramid',
    'waitress',
    'pyramid_tm',
    'psycopg2',
    'sqlalchemy',
    'alembic',
    'zope.sqlalchemy',
    'pyramid_rpc',
    'pyramid_jinja2',
    'pyramid_retry',
    'traitlets',
    'ipython',
    'flake8',
    'pytest',
    'cornice',
    'marshmallow',
    'redis',
    'pyramid_celery',
    'requests',
]

dev_requires = [
    'pyramid_debugtoolbar',
    'pylint',
    'ipdb',
]

setup(
    name='currency',
    version='0.1',
    description='currency aggregator',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Pyramid',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    author='Yushin Andrey',
    author_email='levtolstoy123123@gmail.com',
    url='',
    keywords='web pyramid pylons',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    extras_require={
        'dev': dev_requires
    },
    entry_points={
        'paste.app_factory': [
            'main = currency:main'
        ],
        'console_scripts': [
            'init_db = currency.scripts.init_db:main',
            'shell = currency.scripts.shell:main',
            'fetch_nbu = currency.scripts.fetch_nbu_history:main'
        ],
    },
)
