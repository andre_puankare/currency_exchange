from pyramid.paster import bootstrap
from pyramid_celery import celery_app as app

import requests

from ..schema.nbu import NBUExchangeSchema
from ..models.nbu import NBUExchange


@app.task
def fetch_stats_data(config_uri):
    url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'

    sc = NBUExchangeSchema()
    env = bootstrap(config_uri)
    with env['request'].tm:
        db = env['request'].db
        print('start..')
        data = sc.loads(requests.get(url).content, many=True)
        insert_data = [NBUExchange(**fields) for fields in data]
        for item in insert_data:
            db.add(item)
        print('%s inserted' % len(insert_data))
        print('end..')
