from datetime import datetime


def validate_date(request, **kwargs):
    str_date = request.params.get('date')
    try:
        date = datetime.strptime(str_date, '%Y%m%d')
    except (ValueError, TypeError) as err:
        request.errors.add(
            'querystring',
            'Wrong parameters format:',
            'date: "%s" must be in %%Y%%m%%d format' % str_date,
        )
    else:
        request.validated['date'] = date.replace(
            hour=0,
            minute=0,
            second=0,
            microsecond=0,
        )


def validate_valcode(request, **kwargs):
    try:
        request.validated['valcode'] = \
            request.params.get('valcode').upper()
    except AttributeError:
        pass
