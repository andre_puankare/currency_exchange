from marshmallow.fields import (
    Integer,
    String,
    Date,
    Float,
)
from marshmallow import Schema


class NBUExchangeSchema(Schema):
    pk = Integer(required=False)
    r030 = Integer(required=True)
    txt = String(required=True)
    rate = Float()
    cc = String()
    exchangedate = Date('%d.%m.%Y')
