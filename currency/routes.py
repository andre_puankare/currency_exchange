def includeme(config):
    config.add_route('index', '/')
    config.add_route('nbu', '/nbu')

    config.add_jsonrpc_endpoint('rpc', '/rpc')
    config.add_static_view(
        name='static',
        path='currency:static',
        cache_max_age=3600,
    )
