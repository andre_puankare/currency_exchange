import argparse
import sys

from datetime import datetime
from datetime import timedelta
from time import sleep

from pyramid.paster import bootstrap, setup_logging

from sqlalchemy.exc import OperationalError
from marshmallow import ValidationError
from requests.exceptions import BaseHTTPError

import requests


from ..schema.nbu import NBUExchangeSchema
from ..models.nbu import NBUExchange


def fetch_from_date(tm, db, from_date):
    date_format = '%Y%m%d'
    date_str = from_date
    fetch_date = datetime.strptime(date_str, date_format)
    now = datetime.now()

    while fetch_date < now:
        url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json&date=%s' % (
            fetch_date.strftime(date_format),
        )
        print('start fetching date - %s\n%s' % (
            fetch_date.strftime(date_format),
            url,
        ))
        sc = NBUExchangeSchema()
        try:
            content = requests.get(url).content
        except BaseHTTPError as err:
            print(err)
            continue
        try:
            data = sc.loads(content, many=True)
        except ValidationError as err:
            print(err)
            continue
        print('try add to database...')
        with tm:
            data_to_insert = [NBUExchange(**fields) for fields in data]
            for item in data_to_insert:
                db.add(item)
            print('%s inserted' % len(data_to_insert))
        fetch_date += timedelta(days=1)
        print('end..')


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'config_uri',
        help='Configuration file, e.g., development.ini',
    )
    parser.add_argument(
        '-f', '--from_date',
        default='19970101',
        help='Fetch data from date. Example format 19970101',
    )
    return parser.parse_args(argv[1:])


def main(argv=sys.argv):
    args = parse_args(argv)
    setup_logging(args.config_uri)
    env = bootstrap(args.config_uri)
    try:
        db = env['request'].db
        fetch_from_date(env['request'].tm, db, args.from_date)
    except OperationalError:
        print('''
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:
1.  You may need to initialize your database tables with `alembic`.
    Check your README.txt for description and try to run it.
2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.
            ''')
