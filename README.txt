currency agregator
=============

Getting Started
---------------

- Change directory into your newly created project.

    cd currency

- Create a Python virtual environment.

    python3 -m venv env

- Upgrade packaging tools.

    env/bin/pip install --upgrade pip setuptools

- Install the project in editable mode with its testing requirements.

    env/bin/pip install -e ".[dev]"

- Initialize and upgrade the database using Alembic.

    - Generate your first revision.

        env/bin/alembic -c development.ini revision --autogenerate -m "init"

    - Upgrade to that revision.

        env/bin/alembic -c development.ini upgrade head

- Load default data into the database using a script.

    env/bin/initialize_currency_db development.ini

- Run your project.

    env/bin/pserve development.ini

- Git commands

    git remote -v
    git remote add upstream https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git
    git fetch upstream
    git checkout <branch>
    git rebase upstream/master
    git add <resolved files>
    git rebase --continue or git rebase --skip
    """
    hint: Use 'git am --show-current-patch' to see the failed patch
    Resolve all conflicts manually, mark them as resolved with
    "git add/rm <conflicted_files>", then run "git rebase --continue".
    You can instead skip this commit: run "git rebase --skip".
    To abort and get back to the state before "git rebase", run "git rebase --abort".
    """
    git push origin
    # pull request
    git tag -a v1.0 -m 'version 1.0'

    git push origin --tags

